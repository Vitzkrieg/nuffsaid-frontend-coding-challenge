import React from 'react';
import { mount } from 'enzyme';
import "../../setupTests"

import ControlButtons from './ControlButtons';
import { ApiContext } from '../contexts/ApiContext'
import { MessageProvider } from "../contexts/MessageContext";

const startMessages = () => { }
const stopMessages = () => { }

//api state to pass to button controls
const apiState = {
    started: true,
    start: startMessages,
    stop: stopMessages
}

describe('ControlButtons Component', function () {

    test('Clear button renders with "Clear" as title', () => {
        const wrapper = mount(
            <ApiContext.Provider value={apiState}>
                <ControlButtons />
            </ApiContext.Provider>
        );
        const button = wrapper.find('button.clear');
        expect(button.text()).toBe('Clear');
    });

    test('Toggle button renders with "Stop" as title', () => {
        const wrapper = mount(
            <ApiContext.Provider value={apiState}>
                <ControlButtons />
            </ApiContext.Provider>
        );
        const button = wrapper.find('button.msg-toggle');
        expect(button.text()).toBe('Stop');
    });

    test('Toggle button renders with "Start" as title', () => {
        const apiStateStart = { ...apiState, started: false }
        const wrapper = mount(
            <ApiContext.Provider value={apiStateStart}>
                <ControlButtons />
            </ApiContext.Provider>
        );
        const button = wrapper.find('button.msg-toggle');
        expect(button.text()).toBe('Start');
    });

    it('Clear button should call "clearAllMessages" function', () => {
        const fn = jest.fn();
        const msgState = {
            clearAllMessages: fn
        }
        const wrapper = mount(
            <MessageProvider value={msgState}>
                <ApiContext.Provider value={apiState}>
                    <ControlButtons />
                </ApiContext.Provider>
            </MessageProvider>
        );
        wrapper.find('button.clear').simulate('click');
        expect(fn).toHaveBeenCalled();
    });

    it('Toggle button should call "stop" function', () => {
        const fn = jest.fn();
        const apiStateStop = { ...apiState, stop: fn }
        const wrapper = mount(
            <MessageProvider>
                <ApiContext.Provider value={apiStateStop}>
                    <ControlButtons />
                </ApiContext.Provider>
            </MessageProvider>
        );
        wrapper.find('button.msg-toggle').simulate('click');
        expect(fn).toHaveBeenCalled();
    });

    it('Toggle button should call "start" function', () => {
        const fn = jest.fn();
        const apiStateStart = { ...apiState, started: false, start: fn }
        const wrapper = mount(
            <MessageProvider>
                <ApiContext.Provider value={apiStateStart}>
                    <ControlButtons />
                </ApiContext.Provider>
            </MessageProvider>
        );
        wrapper.find('button.msg-toggle').simulate('click');
        expect(fn).toHaveBeenCalled();
    });
});




