import React from 'react';
import { mount } from 'enzyme';
import "../../setupTests"

import Header from './Header';

describe('Header Component', function () {
    test('Header renders the title inside it', () => {
        const wrapper = mount(
            <Header />
        );
        const title = wrapper.find('h4');
        expect(title.text()).toBe('nuffsaid.com Coding Challenge');
    });
});