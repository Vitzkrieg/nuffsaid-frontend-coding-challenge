import React, { useContext } from "react";

import { MessageContext } from "../contexts/MessageContext";
import { ThemeContext } from '../contexts/ThemeContext';

import {
    Button,
    Card,
    CardContent,
    CardActions,
    Grid,
    Typography
} from "@material-ui/core";


function ListColumn({ data }) {

    const { type, title, priority } = data;

    const { msgtypeList, clearMessage } = useContext(MessageContext);
    const { theme } = useContext(ThemeContext);

    const myList = (msgtypeList && msgtypeList[priority]) ? [...msgtypeList[priority]] : [];

    const renderList = (list) => {

        if (!list || !list.length) {
            return;
        }

        return (list.map((message, index) => {

            if (!message) {
                return null;
            }

            const className = 'list-column list-column-' + type;

            return (
                <Grid
                    item
                    key={index}>
                    <Card
                        className={className}
                        style={{
                            backgroundColor: theme[type],
                            marginBottom: theme.messages.container.marginBottom
                        }}
                    >
                        <CardContent style={theme.messages.message}>
                            <Typography>
                                {message.message}
                            </Typography>
                        </CardContent>
                        <CardActions style={theme.messages.action}>
                            <Button
                                size="small"
                                onClick={() => {
                                    clearMessage(priority, message)
                                }}>Clear</Button>
                        </CardActions>
                    </Card >
                </Grid >
            );
        })
        )
    }


    return (
        <Grid
            item
            xs={4}
            key={type ? type : Math.floor(Math.random() * 100)} >
            <Typography variant="h6">{title ? title : "List"}</Typography>
            <Typography variant="subtitle2">Count: {myList.length}</Typography>
            <Grid
                container
                direction="column">
                {renderList(myList)}
            </Grid>
        </Grid >
    )

}


export default ListColumn;