import { useContext } from 'react'
import { Grid, Typography } from '@material-ui/core'

import { ThemeContext } from '../contexts/ThemeContext';


function Header() {

    const { theme } = useContext(ThemeContext);

    return (
        <Grid
            container
            direction="row"
            style={theme.header.container}
        >
            <Grid item>
                <Typography variant="h4"
                    style={theme.header.title}>nuffsaid.com Coding Challenge</Typography>
            </Grid>
        </Grid>
    )

}

export default Header
