import React, { useContext, useEffect, useState } from 'react'
import {
  Grid
} from '@material-ui/core'

import Api from '../api'

import { MessageContext } from '../contexts/MessageContext';
import { ThemeContext, themes } from '../contexts/ThemeContext';
import { ApiContext } from '../contexts/ApiContext'

import MessageTypes from '../constants/MessageTypes'

import Header from './Header';
import ListColumn from './ListColumn';
import ControlButtons from './ControlButtons';
import ErrorSnack from './ErrorSnack';



function MessageList() {

  const { addMessage } = useContext(MessageContext)

  const [isApiStarted, setApiStarted] = useState(ApiContext.started);
  const [newMessage, setNewMessage] = useState(null);
  //added api to state to avoid creating a new one on refresh
  const [api, setApi] = useState(new Api({
    messageCallback: (message) => {
      setNewMessage(message);
    }
  }));

  //start api on first render
  useEffect(() => {
    api.start();

    return () => {
      api.stop();
    }
  }, [])

  //add new api message to MessageContext for use elsewhere
  useEffect(() => {
    addMessage(newMessage)
  }, [newMessage])

  //start api messages - passed to button controls
  const startMessages = () => {
    api.start();
    setApiStarted(api.isStarted());
  }

  //stop api messages - passed to button controls
  const stopMessages = () => {
    api.stop();
    setApiStarted(api.isStarted());
  }

  //api state to pass to button controls
  const apiState = {
    started: api.isStarted(),
    start: startMessages,
    stop: stopMessages
  }

  return (
    <ThemeContext.Provider value={{ theme: themes.default }}>
      <Grid
        container
        direction="row"
        alignContent="center"
        justify="center">
        <ErrorSnack />
        <Header />
        <ApiContext.Provider value={apiState}>
          <ControlButtons />
        </ApiContext.Provider>
        <Grid
          container
          item
          spacing={16}
          xs={10}>
          {(MessageTypes.map(msgtype => {
            return (
              <ListColumn key={msgtype.type} data={msgtype} ></ListColumn>
            )
          }))}
        </Grid>
      </Grid>
    </ThemeContext.Provider>
  )
}


export default MessageList