import React, { useContext } from 'react'

import { Button, Grid } from '@material-ui/core'
import { MessageContext } from '../contexts/MessageContext'
import { ThemeContext } from '../contexts/ThemeContext'
import { ApiContext } from '../contexts/ApiContext'

function ControlButtons() {

    const { clearAllMessages } = useContext(MessageContext)
    const { theme } = useContext(ThemeContext);


    return (
        <ApiContext.Consumer>
            {({ started, start, stop }) => (
                <Grid
                    container
                    direction="row"
                    alignContent="center"
                    justify="center"
                    spacing={8}
                    style={theme.controls.container}
                >
                    <Grid item>
                        <Button
                            variant="contained"
                            className={'msg-toggle'}
                            onClick={() => {
                                if (started) {
                                    stop()
                                } else {
                                    start()
                                }
                            }}
                            style={theme.controls.buttons}
                        >
                            {started ? 'Stop' : 'Start'}
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            className={'clear'}
                            onClick={() => {
                                clearAllMessages()
                            }}
                            style={theme.controls.buttons}
                        >Clear</Button>
                    </Grid>
                </Grid>
            )}
        </ApiContext.Consumer>
    )
}

export default ControlButtons