import React, { useContext, useEffect } from 'react'

import { MessageContext } from '../contexts/MessageContext'
import { ThemeContext } from '../contexts/ThemeContext'

import {
    Button,
    Snackbar,
    SnackbarContent
} from '@material-ui/core'


function ErrorSnack() {

    const { errorSnack, setErrorSnack } = useContext(MessageContext);
    const { theme } = useContext(ThemeContext);

    const [snackPack, setSnackPack] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [messageInfo, setMessageInfo] = React.useState(null);


    const closeErrorSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    }

    const snackExited = () => {
        setMessageInfo(null);
    }

    useEffect(() => {
        if (!!errorSnack) {
            //add new error to the end of the list
            setSnackPack((prev) => [...prev.slice(0), errorSnack]);
            setErrorSnack(null);
        } else if (snackPack.length && messageInfo && open) {
            //close the current snack to then open the new one
            closeErrorSnack();
        } else if (snackPack.length && !messageInfo) {
            //show next snack
            //create new object to add unique key
            const currSnack = { ...snackPack[0], key: Date.now() };
            setMessageInfo(currSnack);
            setSnackPack((prev) => prev.slice(1));
            setOpen(true);
        }
    }, [snackPack, messageInfo, open, errorSnack]);

    // don't display if no info
    if (!messageInfo) {
        return null;
    }

    return (
        <Snackbar
            key={messageInfo.key}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
            role={'alert'}
            className={'error-snack'}
            open={open}
            autoHideDuration={2000}
            onClose={() => closeErrorSnack()}
            TransitionProps={{
                onExit: () => snackExited()
            }}
            ClickAwayListenerProps={{ mouseEvent: false }}
        >
            <SnackbarContent
                aria-describedby="message-id2"
                style={theme.errorSnack}
                message={messageInfo.message}
                action={
                    < React.Fragment>
                        <Button size="small" onClick={() => closeErrorSnack()}>
                            X
                        </Button>
                    </React.Fragment >
                }
            />
        </Snackbar >
    )
}

export default ErrorSnack