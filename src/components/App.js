import React from "react";

import { MessageProvider } from "../contexts/MessageContext";
import MessageList from "./MessageList";


function App() {

    return (
        <MessageProvider>
            <MessageList />
        </MessageProvider>
    )
}

export default App;