import React from 'react';
import { mount } from 'enzyme';
import "../../setupTests"

import {
    Grid
} from '@material-ui/core'

import { MessageProvider } from "../contexts/MessageContext";
import ListColumn from './ListColumn';

const defaultData = {
    type: 'type',
    title: 'title',
    priority: 0
}

const message = {
    message: 'I am an error',
    priority: 1
}


describe('ListColumn Component', function () {

    test('ListColumn renders null', () => {
        const wrapper = mount(
            <ListColumn data={defaultData} />
        );
        const comp = wrapper.find('.list-column');
        expect(comp[0]).toBeUndefined();
    })

    /*
        test('ListColumn renders with single message', () => {
            const errorData = {
                type: 'error',
                title: 'Errors',
                priority: 1
            }
            const msgData = {
                msgtypeList: {
                    'error': [message]
                }
            }
            const wrapper = mount(
                <MessageProvider value={msgData}>
                    <Grid
                        container
                        item
                        spacing={16}
                        xs={10}>
                        <ListColumn data={errorData} />
                    </Grid>
                </MessageProvider>
            );
    
            const comp = wrapper.find('.list-column');
            expect(comp[0]).toBeDefined();
        });
        */

});