import React from 'react';
import { mount, shallow } from 'enzyme';
import "../../setupTests"

import ErrorSnack from './ErrorSnack';
import { MessageProvider } from "../contexts/MessageContext";
import { MessageContext } from '../contexts/MessageContext';

const msgState = {
    errorSnack: null
}

describe('ErrorSnack Component', function () {

    const message = {
        message: 'Hello to you!',
        priority: 1
    };

    const msgState = {
        errorSnack: message
    }


    test('ErrorSnack renders null', () => {
        const wrapper = mount(
            <MessageProvider value={msgState}>
                <ErrorSnack />
            </MessageProvider>
        );
        const snack = wrapper.find('.error-snack');
        expect(snack[0]).toBeUndefined();
    })


    //TODO: research testing useEffect()


});