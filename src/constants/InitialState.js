export const initState = {
    msgtypeList: [],
    errorSnack: null,
    newMessage: null,
}

export default initState;