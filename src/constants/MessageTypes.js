export const MessageTypes = [
    {
        'type': 'error',
        'title': 'Errors',
        'priority': 1
    },
    {
        'type': 'warning',
        'title': 'Warnings',
        'priority': 2
    },
    {
        'type': 'info',
        'title': 'Info',
        'priority': 3
    }
]

export default MessageTypes;