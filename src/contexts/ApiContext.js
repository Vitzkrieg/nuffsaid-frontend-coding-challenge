import React from 'react'

export const ApiContext = React.createContext({
    started: true,
    start: () => { },
    stop: () => { }
});


export default ApiContext