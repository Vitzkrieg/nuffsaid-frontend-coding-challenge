import React, { useState, useEffect } from 'react'

import { initState } from '../constants/InitialState';


export const msgFunction = (msg) => { };
export const clearFunction = (priority, msg) => { };


export const MessageContext = React.createContext({
    msgtypeList: initState.msgtypeList,
    addMessage: msgFunction,
    clearMessage: clearFunction,
    clearAllMessages: msgFunction,
    errorSnack: initState.errorSnack,
    setErrorSnack: msgFunction,
});

export const MessageProvider = (props) => {

    const [msgtypeList, setMsgtypeList] = useState(initState.msgtypeList);
    const [newMessage, setNewMessage] = useState(initState.newMessage);
    const [errorSnack, setErrorSnack] = useState(initState.errorSnack,)


    const addMessage = (message) => {
        setNewMessage(message);
    };

    const clearMessage = (typeindex, msg) => {

        if (!msgtypeList.length || !msgtypeList[typeindex].length) {
            return;
        }

        const tindex = msgtypeList[typeindex].findIndex((value) => {
            return value && value.message === msg.message;
        })

        if (tindex === -1) {
            return;
        }

        let newMsgType = [...msgtypeList[typeindex]];
        newMsgType.splice(tindex, 1);
        msgtypeList[typeindex] = newMsgType;

        setMsgtypeList(msgtypeList);
    }

    const clearAllMessages = () => {
        setMsgtypeList([]);
        setNewMessage(null);
        setErrorSnack(null);
    }


    useEffect(() => {
        if (!newMessage) {
            return;
        }

        if (newMessage.priority === 1) {
            setErrorSnack(newMessage);
        }

        if (!msgtypeList[newMessage.priority]) {
            msgtypeList[newMessage.priority] = [newMessage];
        } else {
            msgtypeList[newMessage.priority].unshift(newMessage);
        }
        setMsgtypeList(msgtypeList);
    }, [newMessage])

    const state = {
        msgtypeList: msgtypeList,
        addMessage: addMessage,
        clearMessage: clearMessage,
        clearAllMessages: clearAllMessages,
        setErrorSnack: setErrorSnack,
        errorSnack: errorSnack,
        ...props.value,
    }

    return (
        <MessageContext.Provider value={state}>
            {props.children}
        </MessageContext.Provider>
    )
}