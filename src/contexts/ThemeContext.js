import React from 'react'

import { styles } from '../styles';

export const themes = styles;

export const ThemeContext = React.createContext({
    theme: themes.default
});