const Colors = {
    error: '#F56236',
    warning: '#FCE788',
    info: '#88FCA3',
    button: '#00E2C4',
    foreground: '#000000',
    background: '#eeeeee',
}

export default Colors;