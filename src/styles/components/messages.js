export const messages = {
    container: {
        marginBottom: '10px'
    },
    message: {
        paddingBottom: '0px'
    },
    action: {
        float: 'right'
    }
}

export default messages;