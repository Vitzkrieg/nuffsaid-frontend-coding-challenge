import header from './header';
import controls from './controls'
import messages from './messages'
import errorsnack from './errorsnack'


export const components = {
    header: header,
    controls: controls,
    messages: messages,
    errorSnack: errorsnack
}


export default components;