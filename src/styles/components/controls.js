import Colors from "../colors"

export const controls = {
    container: {
        marginBottom: '40px'
    },
    buttons: {
        backgroundColor: Colors.button
    }
}

export default controls;