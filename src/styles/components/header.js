export const header = {
    container: {
        borderBottom: '3px solid grey',
        marginBottom: '1em'
    },
    title: {
        fontWeight: 'normal',
        fontSize: '2em'
    }
}

export default header;