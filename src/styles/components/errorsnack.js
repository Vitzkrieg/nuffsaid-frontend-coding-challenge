import Colors from "../colors"

export const errorsnack = {
    color: Colors.foreground,
    backgroundColor: Colors.error,
    '> *': {
        backgroundColor: Colors.error,
    }
}

export default errorsnack;