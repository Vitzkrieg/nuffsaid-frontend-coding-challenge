import components from "./components";
import Colors from "./colors";


export const general = {
    ...Colors,
    ...components,
}

export default general;